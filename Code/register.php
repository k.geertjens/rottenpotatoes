<?php 
    //misc
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_errors', true );

    //Header
    include_once 'include/header.php';

    //setting the database
    $UMan = new UserManager();
    $usernameErr = $emailErr = $passwordErr = $password2Err = "";

    //Body
    if($user->getId() == -1)
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            if(empty($_POST["username"]) || empty($_POST["email"]) ||empty($_POST["password"]) || empty($_POST["password2"]))
            {
                //wip
                $usernameErr = $emailErr = $passwordErr = $password2Err = "*";
            }
            else
            {
                $password = test_input($_POST["password"]);
                $password2 = test_input($_POST["password2"]);
                $username = test_input($_POST["username"]);
                
                $email = test_input($_POST["email"]);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                {
                    echo "Invalid email format";
                }
                else if($password == $password2)
                {
                    if(!$UMan->CheckEmail($email))
                    {
                        $UMan->AddUser($username,$email,$password);
                        $user = $UMan->Login($email,$password);
                        //setting the sessions
                        $_SESSION[ "email" ] = $email;
                        $_SESSION[ "id" ] = $user->GetId();
                        header("Location: my-account.php");
            
                    }
                    else
                    {
                        $usernameErr = $emailErr = $passwordErr = $password2Err = "*";
                    }
                }
                else
                {
                    $usernameErr = $emailErr = $passwordErr = $password2Err = "*";
                }
            }
        }

        //form
        echo '<script type="text/javascript" src="javascript/formValidation.js"></script>';
        
        echo '<div class="login">';
        echo '<form method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" name="registerForm" onsubmit="return validateRegisterForm()">';//was injectie gevoelig nu niet meer lmao
        echo '<a>Username</a> <br>';
        echo '<input id="Lemail" type="text" placeholder="Username" name="username">';
        echo '<span class="error">'.$usernameErr.'</span><br>';
        echo '<a>Email</a> <br>';
        echo '<input id="Lemail" type="text" placeholder="Email" name="email">';
        echo '<span class="error">'.$emailErr.'</span><br>';
        echo '<a>Password</a> <br>';
        echo '<input id="Lpassword" type="text" placeholder="Password" name="password">';
        echo '<span class="error">'.$passwordErr.'</span><br>';
        echo '<a>Confirm Password</a> <br>';
        echo '<input id="Lpassword" type="text" placeholder="Confirm Password" name="password2">';
        echo '<span class="error">'.$password2Err.'</span><br><br>';
        echo '<input type="submit" name="submit" value="Register"><br><br>';
        echo '</form>';
        echo '</div>';
    }
    else
    {
        //cant acces this page if you are logged in
        header("Location: my-account.php");
        exit();
    }



    //Footer
    include_once 'include/footer.php';
?> 
