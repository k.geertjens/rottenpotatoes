<?php
    class Report
    {
        //Properties
        private $id;

        private $game;
        private $user;
        private $report;


        //Constructor
        public function __construct(int $id, int $game, int $user, string $report)
        {
            $this->id = $id;

            $this->game = $game;
            $this->user = $user;
            $this->report = $report;

        }

        //Get-methods
        public function getId() { return $this->id; }
        public function getGame() { return $this->game; }
        public function getUser() { return $this->user; }
        public function getReport() { return $this->report; }



    }
?>