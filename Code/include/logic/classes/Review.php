<?php
    class Review
    {
        //Properties
        private $id;
        private $user_id;
        private $game_id;
        private $title;
        private $description;
        private $rating;

        //Constructor
        public function __construct(int $id, int $user_id, int $game_id, string $title, string $description, int $rating)
        {
            $this->id = $id;
            $this->user_id = $user_id;
            $this->game_id = $game_id;
            $this->title = $title;
            $this->description = $description;
            $this->rating = $rating;
        }

        //Functions
        public function GetId() { return $this->id; }
        public function GetUserId() { return $this->user_id; }
        public function GetGameId() { return $this->game_id; }
        public function GetTitle() { return $this->title; }
        public function GetDescription() { return $this->description; }
        public function GetRating() { return $this->rating; }
    }
?>