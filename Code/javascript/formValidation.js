//This file includes the validation functions for all forms
function validateContactForm()
{
    var emailInput = document.forms["contactForm"]["email"].value;
    var subjectInput = document.forms["contactForm"]["subject"].value;
    var messageInput = document.forms["contactForm"]["message"].value;

    if(emailInput == "" || messageInput == "" || subjectInput == "")
    {
        alert("All fields must be filled out");
        return false;
    }
    else
    {
        alert("Email sent successfully");
        return true;
    }
}

function validateAddGameForm()
{
    var titleInput = document.forms["addGameForm"]["title"].value;
    var descriptionInput = document.forms["addGameForm"]["description"].value;
    var pictureInput = document.forms["addGameForm"]["file"].value;
    var publisherInput = document.forms["addGameForm"]["publisher"].value;
    var releaseDateInput = document.forms["addGameForm"]["releaseDate"].value;
    var genreInput = document.forms["addGameForm"]["genre"].value;

    var cpuInput = document.forms["addGameForm"]["cpu"].value;
    var gpuInput = document.forms["addGameForm"]["gpu"].value;
    var ramInput = document.forms["addGameForm"]["ram"].value;
    var storageInput = document.forms["addGameForm"]["storage"].value;
    var osInput = document.forms["addGameForm"]["os"].value;

    if(titleInput == "" || descriptionInput == "" || pictureInput == "" || publisherInput == "" || releaseDateInput == "" || genreInput == "" || cpuInput == "" || gpuInput == "" || ramInput == "" || storageInput == "" || osInput == "")
    {
        alert("All fields must be filled out");
        return false;
    }
    else
    {
        if(pictureInput.includes(".jpg") || pictureInput.includes(".jpeg"))
        {
            alert("Request made successfully");
            return true;
        }
        else
        {
            alert("Picture must be of type .jpg or .jpeg");
            return true;
        }
    }
}

function validateCreateReviewForm()
{
    var titleInput = document.forms["createReviewForm"]["title"].value;
    var ratingInput = document.forms["createReviewForm"]["rating"].value;
    var descriptionInput = document.forms["createReviewForm"]["description"].value;

    if(titleInput == "" || ratingInput == "" || descriptionInput == "")
    {
        alert("All fields must be filled out");
        return false;
    }
    else
    {
        alert("Review made successfully");
        return true;
    }
}

function validateLoginForm()
{
    var emailInput = document.forms["loginForm"]["email"].value;
    var passwordInput = document.forms["loginForm"]["password"].value;

    if(emailInput == "" || passwordInput == "")
    {
        alert("All fields must be filled out");
        return false;
    }
    else
    {
        return true;
    }
}

function validateEditAccountForm()
{
    var usernameInput = document.forms["editAccountForm"]["username"].value;
    var emailInput = document.forms["editAccountForm"]["email"].value;
    var passwordInput = document.forms["editAccountForm"]["password"].value;

    if(emailInput == "" || passwordInput == "" || usernameInput == "")
    {
        alert("All fields must be filled out");
        return false;
    }
    else
    {
        return true;
    }
}

function validateRegisterForm()
{
    var usernameInput = document.forms["registerForm"]["username"].value;
    var emailInput = document.forms["registerForm"]["email"].value;
    var passwordInput = document.forms["registerForm"]["password"].value;
    var passwordInput2 = document.forms["registerForm"]["password2"].value;

    if(emailInput == "" || passwordInput == "" || usernameInput == "" || passwordInput2 == "")
    {
        alert("All fields must be filled out");
        return false;
    }
    else
    {
        alert("Account registered successfully");
        return true;
    }
}

function validateReportForm()
{
    var reportInput = document.forms["ReportForm"]["report"].value;

    if(reportInput == "" )
    {
        alert("All fields must be filled out");
        return false;
    }
    else
    {
        alert("Reported");
        return true;
    }
}