<?php
    require_once dirname(__DIR__).'/logic/misc/Misc.php';

    AutoLoad("report.php");
    AutoLoad("DBGame.php");//this can be done better lol

    class DBReport
    {
        private $username;
        private $password;
        private $conn_string;

        public function __construct()
        {
            $this->username = USERNAME;
            $this->password = PASSWORD;
            $this->conn_string = CONN_STRING;
        }

        public function GetReports(int $game_ID)
        {
            $reports = array();
            
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, loading games
                $query = "SELECT * FROM report where Game_ID = :game_id";
                $sth = $conn->prepare($query);

                $sth->execute([':game_id' => $game_ID]);

                foreach($sth->fetchall() as $row)
                {
                    $report = new report(intval($row['ID']),intval($row['Game_ID']),intval($row['User_ID']),$row['Report']) ;

                    array_push($reports, $report);
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning Array
            return $reports;
        }

        public function AddReport(int $game_id, int $user_id, string $report)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                //Connection established, doing actions

                
                $query = "INSERT INTO `report` (`ID`, `User_ID`, `Game_ID`, `Report`) VALUES (NULL, :user, :game, :report)";
                $sth = $conn->prepare($query);

                $sth->execute([':game' => $game_id, ':user' => $user_id, ':report' => $report]);
                
                //Closing Connection
                $conn = null;

                //updating the game table
                $DBGame = new DBGame();
                $DBGame->ReportGame($game_id);
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }

        public function RemoveReports(int $game_id)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                //Connection established, doing actions
                
                $query = "DELETE FROM `report` WHERE Game_ID = :id";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $game_id]);
                
                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }
    }

?>