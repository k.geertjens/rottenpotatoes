<?php 
    //Including classes
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_errors', true );

    //Header
    include_once 'include/header.php';

    //classes
    require_once 'include/logic/classes/Review.php';
    require_once 'include/logic/classes/ReviewManager.php';
    require_once 'include/logic/classes/Game.php';
    require_once 'include/logic/classes/GameManager.php';
?>


<br>
<br>

<h2>Your Reviews</h2>

<br>
<br>

<?php
    $reviewManager = new ReviewManager();
    $reviewManager->LoadReviewsByUser($_SESSION["id"]);

    $gameManager = new GameManager();
    $gameManager->LoadGames();

    //div of all the games
    echo '<div class="boxes">';

    //count for puting the reviews in the right divs
    $count = 0;

    //loops through the reviews we have
    foreach($reviewManager->GetReviews() as $review)
    {
        //every 2 games is a new row
        if ($count %2 == 0)
        {
            echo'<div class="row">';
        }

        $game = $gameManager->GetGame($review->GetGameId());

        echo '<div class= "game-box">';
        echo '<div class= "homepage_img">';

        echo '<img src="game_images/'.$game->getImage().'"/>';
        echo '</div>';

        echo '<div class="card_description">';
        echo '<h2>'.$review->getTitle().'</h2>';
        echo '<b>Game: '.$game->getTitle().'</b> <br>';
        echo '<b>Rating: '.$review->GetRating().'</b> <br>';
        
        $description = $review->GetDescription();
        echo '<p>'.$description.'</p>';
        echo '</div>';

        echo "<div class='ReadMore'>";
        echo '<a href="gamePage.php?game_id='.$game->getId().'">Read More</a>';
        echo '</div>';

        echo '</div>';
        echo "<br>";

        //every 2 games is a new row end tag
        if ($count %2 == 1)
        {
            echo'</div>';
            echo'<br>';
        }
        $count++;
    }

    //if there is an oneven amount of games we still need to close it
    if ($count %2 == 1)
    {
        echo'</div>';
    }
    echo '</div>';
?>
<br>

<?php
    //Footer
    include_once 'include/footer.php';
?> 