<?php
    require_once 'include/logic/misc/Misc.php';

    AutoLoad('Game.php');
    AutoLoad('DBGame.php');

    class GameManager
    {
        //Properties
        private $games;
        private $DBHelper;

        //Constructor
        public function __construct()
        {
            $this->games = array();
            $this->DBHelper = new DBGame();
        }
        
        //Get-methods
        public function GetGames() { return $this->games; }
        public function GetGame(int $id)
        {
            foreach($this->games as $game)
            {
                if($game->getId() == $id) return $game;
            }
            return null;
        }

        //Queries
        public function LoadGames()
        {
            $this->games = $this->DBHelper->GetGames();
        }


    }
?>