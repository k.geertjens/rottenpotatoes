<?php 
    //Including classes
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_errors', true );

    //Header
    include_once 'include/header.php';

    //classes
    require_once 'include/logic/classes/Game.php';
    require_once 'include/logic/classes/GameManager.php';

    AutoLoad('Game.php');
    AutoLoad('GameManager.php');
?>


<br>
<br>

<h3>Welcome to RottenPotatoes</h3>
<p>Hey there, welcome to RottenPotatoes, the game review website for you! Want to know more about us? Go to <a href="about-us.php">this</a> page to read up on us.</p>
<p>Alternatively, check out one of our many games in the list below. We hope you enjoy your stay!</p>

<br>
<br>
<h2>Games</h2>

<?php
    $gameManager = new GameManager();
    $gameManager->LoadGames();
//still needs an picture and a butten and the text needs to be schrunken

    //div of all the games
    echo '<div class="boxes">';

    //count for puting the games in the right divs
    $count = 0;

    //loops through the games we have
    foreach($gameManager->GetGames() as $game)
    {
        //every 2 games is a new row
        if ($count %2 == 0)
        {
            echo'<div class="row">';
        }

        echo '<div class= "game-box">';
        echo '<div class= "homepage_img">';

        echo '<img src="game_images/'.$game->getImage().'"/>';
        echo '</div>';

        echo '<div class="card_description">';
        echo '<h2>'.$game->getTitle().'</h2>';
        //echo '<b>'.$game->getPublisher().'</b> <br>';
        //echo '<b>'.$game->getReleaseDate()->format('d-m-Y').'</b> <br>';
        
        $description = substr($game->GetDescription(), 0, 300)."...";
        echo '<p>'.$description.'</p>';
        echo '</div>';

        echo "<div class='ReadMore'>";
        echo '<a href="gamePage.php?game_id='.$game->getId().'">Read More</a>';
        echo '</div>';

        echo '</div>';
        echo "<br>";

        //every 2 games is a new row end tag
        if ($count %2 == 1)
        {
            echo'</div>';
            echo'<br>';
        }
        $count++;
    }

    //if there is an oneven amount of games we still need to close it
    if ($count %2 == 1)
    {
        echo'</div>';
    }
    echo '</div>';
?>
<br>

<?php
    //Footer
    include_once 'include/footer.php';
?> 

