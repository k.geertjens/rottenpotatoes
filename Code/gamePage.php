<?php 
    //misc
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_errors', true );

    //Header
    include_once 'include/header.php';
    
    AutoLoad('Game.php');
    AutoLoad('GameManager.php');
    AutoLoad('Review.php');
    AutoLoad('ReviewManager.php');

    AutoLoad('DBReport.php');

    echo '<script type="text/javascript" src="javascript/formValidation.js"></script>';

    
?>

<div class = "gamepage">
    <?php
        //Creating game manager object & loading data
        $gameManager = new GameManager();
        $gameManager->LoadGames();
        $DBGame = new DBGame();

        $userManager = new UserManager();

        //Selecting a specific game from the dataset
        $game_id = $_GET["game_id"];
        $game = $gameManager->GetGame($game_id);
        if($game != null)
        {   
            //creating a report if it needs to be created
            if ($_SERVER["REQUEST_METHOD"] == "POST") 
            {
                if(isset($_POST["report_text"]))
                {
                    $DBReport = new DBReport();
                    $DBReport->AddReport($game_id, $user->GetId(),test_input($_POST["report_text"]));
                }
            }

            //Creating ReviewManager
            $reviewManager = new ReviewManager();

            //Adding review if user made one
            if(isset($_POST['rating']) && !empty($_POST['title']) && !empty($_POST['description']))
            {
                $reviewManager->AddReview($user->getId(), $game_id, $_POST["title"], $_POST["description"], $_POST["rating"]);
            }

            //Loading Reviews
            $reviewManager->LoadReviewsByGame($game_id);
            $avgRating = $reviewManager->GetAvgRating($game_id);

            //Image
            $image = "game_images/".$game->getImage();
            echo "<div class='game_left'>";
           
            echo "<div class='game_image'>";
            echo "<img src='$image' alt=''>";
            echo "</div>";
           
            //System specs
            echo "<div class='game_specs'>";
            echo "<h4>System Requirements</h4>";
            echo "<p>CPU: ".$game->getCPU()."</p>";
            echo "<p>GPU: ".$game->getGPU()."</p>";
            echo "<p>RAM: ".$game->getRAM()."</p>";
            echo "<p>Disk Space: ".$game->getDiskSpace()."</p>";
            echo "<p>OS: ".$game->getOS()."</p>";

            //Report button
            if ($user->getId() != -1)
            {
                echo "<form name='ReportForm' method='post' action='gamePage.php?game_id=$game_id' onsubmit='return validateReportForm()'>";
                echo '<input type="submit" id="submit" name="report" value="Report Game">';
                echo '<input type="text" name="report_text" placeholder="Reason for report">';
                echo "</form>";
            }
            

            echo "</div>";
            echo "</div>";
            
            echo"<div class='game_right'>";
            
            //Title
            echo "<div class='game_title'>";
            echo "<h1>".$game->getTitle()."</h1>";

            //Sub-title
            echo "<div class='game_rating'>";
            echo "<b>Rating: ".floatval($avgRating)."</b>";
            echo "<b>Publisher: ".$game->getPublisher()."</b>";
            echo "<b>Release Date: ".date_format($game->getReleaseDate(), 'd-m-Y')."</b>";
            echo "</div>";
            echo "</div>";

            //Description
            echo "<p>".$game->getDescription()."</p>";
            echo "</div>";

            
            
            //Reviews
            echo "<div class= 'gameReviews'>";
            echo "<h1>Reviews</h1>";

            echo '<script type="text/javascript" src="javascript/formValidation.js"></script>';

            if($user->getId() != -1)
            {
                echo "<div class='review'>";
                echo "<form method='post' action='gamePage.php?game_id=$game_id' name='createReviewForm' onsubmit='return validateCreateReviewForm()'>";
                echo "<h3>Leave a review</h3>";

                echo '<br>';

                echo '<p>Title:</p>';
                echo '<input type="text" name="title">';
                echo '<br>';

                echo '<p>Rating: </p>';
                echo '<input type="radio" id="rating" name="rating" value="1">';
                echo '<input type="radio" id="rating" name="rating" value="2">';
                echo '<input type="radio" id="rating" name="rating" value="3">';
                echo '<input type="radio" id="rating" name="rating" value="4">';
                echo '<input type="radio" id="rating" name="rating" value="5">';

                echo '<br>';

                echo '<p>Description:</p>';
                echo "<textarea name='description'></textarea>";

                echo '<br>';

                echo '<input type="submit" id="submit" value="Submit">';
                echo "</form>";
                echo "</div>";
            }

            foreach($reviewManager->GetReviews() as $review)
            {
                $image = $userManager->GetUserById($review->GetUserId())->getImage();

                echo "<div class='review'>";

                echo '<div class="reviewImage">';
                echo "<img src='".$image."' alt='src='user_images/user.jpg'>";
                echo "</div>";

                echo "<div class='reviewBody'>";

                echo "<h3>".$review->GetTitle()."</h3>";
                echo "<p><i>".$userManager->GetUserById($review->GetUserId())->GetUserName()."</i></p>";
                echo '<br>';
                echo "<b>Rating: ".$review->GetRating()."</b>";
                echo "<p>".$review->GetDescription()."</p>";

                echo "</div>";
                echo "</div>";
            }
            echo "</div>";
        }
        else echo "No Game Found";
    ?>
    <br>
</div>
<?php
    //Footer
    include_once 'include/footer.php';
?> 
