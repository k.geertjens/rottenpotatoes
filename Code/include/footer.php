                </div>
            <footer class="siteFooter"> 
                <div class="navigation">
                    <div class="row-left">
                        <u1 class="list">
                            <li><a class="footer" href="contact.php">Contact</a></li>
                            <li><a class="footer"  href="about-us.php">About Us</a></li>
                        </u1>
                    </div>
                    <div class="row-right">
                        <u1 class="list">
                        <?php
                            if($user->getId() != -1)
                            {
                                echo '<li><a class="footer" href="my-reviews.php">My Reviews</a></li>';
                                echo '<li><a class="footer" href="add-game.php">Add Game</a></li>';
                            }
                        ?>
                        </u1>
                    </div>

                    <div class="row-right">
                        <u1 class="list">
                        <?php
                            if($user->getId() != -1)
                            {
                                echo '<li><a class="footer" href="index.php">Report Game</a></li>';
                                echo '<li><a class="footer" href="my-account.php">My Account</a></li>';
                            }
                        ?>
                        </u1>
                    </div>
                    <div class="row-right">
                        <u1 class="list">
                            <?php
                            if($user->getId() == -1) echo '<li><a class="footer" href="register.php">Register</a></li>';
                            ?>
                        </u1>
                    </div>

                    <div class="row-right">
                        <u1 class="list">
                            <?php
                            if($user->getId() != -1 && $user->IsAdmin()) echo '<li><a class="footer" href="user-requests.php">User Requests</a></li>';
                            ?>
                        </u1>
                    </div>
                </div>
            </footer >
        </div>
    </body>
</html>