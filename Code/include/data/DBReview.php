<?php
    require_once dirname(__DIR__).'/logic/misc/Misc.php';
    AutoLoad('Review.php');

    class DBReview
    {
        private $username = USERNAME;
        private $password = PASSWORD;
        private $conn_string = CONN_STRING;

        public function AddReview(int $user_id, int $game_id, string $title, string $description, int $rating)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                $query = "INSERT INTO review (user_id, game_id, title, description, rating) VALUES (:user_id, :game_id, :title, :description, :rating)";
                
                $sth = $conn->prepare($query);
                $sth->execute([':user_id' => $user_id, ':game_id' => $game_id, ':title' => $title, ':description' => $description, ':rating' => $rating]);
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }

        public function GetAvgRating(int $game_id)
        {
            $average = 0;

            //$conn = mysqli_connect("studymysql01.fhict.local", "dbi464439", "Portal_21", "dbi464439");
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, getting data
                $average = 0;
                $query = "SELECT AVG(rating) as avg FROM review WHERE game_id = :game_id";
                $sth = $conn->prepare($query);

                $sth->execute([':game_id' => $game_id]);
                foreach($sth->fetchall() as $row)
                {
                    $average = $row['avg'];
                }
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning Result
            return $average;
        }

        public function GetReviewsByGame(int $game_id)
        {
            //Re-instatiating array
            $reviews = array();

            //Connecting to database
            //$conn = mysqli_connect("studymysql01.fhict.local", "dbi464439", "Portal_21", "dbi464439");
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, getting data
                $query = "SELECT * FROM review WHERE game_id = :game_id";
                $sth = $conn->prepare($query);

                $sth->execute([':game_id' => $game_id]);
                foreach($sth->fetchall() as $row)
                {
                    $review = new Review($row['id'], $row['user_id'], $row['game_id'], $row['title'], $row['description'], $row['rating']);
                    array_push($reviews, $review);
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning array
            return $reviews;

        }

        public function GetReviewsByUser(int $user_id)
        {
            //Re-instatiating array
            $reviews = array();

            //Connecting to database
            //$conn = mysqli_connect("studymysql01.fhict.local", "dbi464439", "Portal_21", "dbi464439");
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, getting data
                $query = "SELECT * FROM review WHERE user_id = :user_id";
                $sth = $conn->prepare($query);

                $sth->execute([':user_id' => $user_id]);
                foreach($sth->fetchall() as $row)
                {
                    $review = new Review($row['id'], $row['user_id'], $row['game_id'], $row['title'], $row['description'], $row['rating']);
                    array_push($reviews, $review);
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning array
            return $reviews;
        }
    }

?>