<?php
    require_once dirname(__DIR__).'/logic/misc/Misc.php';

    class DBGenre
    {
        private $username;
        private $password;
        private $conn_string;

        public function __construct()
        {
            $this->username = USERNAME;
            $this->password = PASSWORD;
            $this->conn_string = CONN_STRING;
        }

        public function GetGenre()
        {
            $genres = array();
            
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, loading games
                $query = "SELECT * FROM genre";
                foreach($conn->query($query) as $row)
                {
                    $genre = $row['genre'];

                    array_push($genres, $genre);
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning Array
            return $genres;
        }

        public function AddGenre(string $genre,string $id)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                //Connection established, doing actions

                $query = "SELECT AUTO_INCREMENT
                from information_schema.TABLES
                WHERE TABLE_SCHEMA = 'rottenpotatoes'
                and TABLE_NAME = 'game'";

                $image = "0.jpg";

                foreach($conn->query($query) as $row)
                {
                    $image = $row['AUTO_INCREMENT'];
                }
                
                $query = "INSERT INTO `game_genre`(`game_id`, `genre`) VALUES (:id,:genre ";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id, ':genre' => $genre]);
                
                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

        }
    }

?>