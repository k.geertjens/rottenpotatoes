<!DOCTYPE html>
<?php
    require_once 'include/logic/misc/Misc.php';
    AutoLoad('User.php');
    AutoLoad('UserManager.php');

    //include the header logic
    AutoInclude('HeaderLogic.php');

    include 'include/logic/misc/HeaderLogic.php';
    

?>

<html>
    <script type="text/javascript" src="javascript/searchbar.js"></script>

    <head>
        <title>&#129364; RottenPotatoes &#129364;</title>
        <link rel="stylesheet" href="styling/style.css">
        <link rel="stylesheet" href="styling/style2.css">
    </head>
    <body class="body">
        <div class="gridContainer">
        <header>
            <div class="siteHeader">
                <img src="images/BannerBG.png"/>

                <div class="siteHeaderText">
                    <h1>&#129364; RottenPotatoes &#129364; </h1>
                    <p>Your game review website</p>
                </div>
            </div>
            <div class="nav-bar">
                <a class="header" id="home" href="index.php">Home</a>
                <!-- <input id="searchbarInput" name="searchbarInput" type="text" placeholder="Search Game.." oninput="return search()"> -->

                <?php
                    //the '?'.time(). is to force the webbrowser to update the image every time it is loaded
                    echo "<img id='float-right' src='".$user->getImage().'?'.time()."' alt='src='user_images/user.jpg'>";

                    if ($user->getID() == -1)
                    {
                        $name = "log in";
                    }
                    else
                    {
                        $name = $user->getUsername();
                    }
                    echo "<a class='log-in' id='float-right' style='margin-top: 7px;' href='my-account.php'>".$name. "</a>";
                ?>
            </div>
        </header>
    <div class="main">
        
    

