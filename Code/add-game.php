<?php 
    //misc
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_errors', true );

    //Header
    include_once 'include/header.php';
    AutoLoad("DBGame.php");
    AutoLoad("DBGenre.php");

    //setting the database 
    $DBGame = new DBGame();
    $DBGenre = new DBGenre();
    $titleErr = $descriptionErr = $publisherErr = $releaseDateErr = $compErr= $piccErr = $action= "";
    /*$genres = array();
    if (isset($_SESSION[ "genre" ]))
    {
        $genres = $_SESSION[ "genre" ];
    }*/

    //Body
    if($user->getId() != -1)
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            if (isset($_POST['submit']))
            {
                if(empty($_POST["title"]) || empty($_POST["description"]) ||empty($_POST["publisher"]) || empty($_POST["releaseDate"])
                || empty($_POST["cpu"])|| empty($_POST["gpu"])|| empty($_POST["ram"])|| empty($_POST["storage"])|| empty($_POST["os"]))
                {
                    //wip
                    $titleErr = $descriptionErr = $publisherErr = $releaseDateErr = $compErr= $piccErr = "*";
                }
                else
                {
                    $title = test_input($_POST["title"]);
                    $description = test_input($_POST["description"]);
                    $publisher = test_input($_POST["publisher"]);
                    $releaseDate = test_input($_POST["releaseDate"]);
                    $genre = $_POST["genre"];
    
                    $cpu = test_input($_POST["cpu"]);
                    $gpu = test_input($_POST["gpu"]);
                    $ram = test_input($_POST["ram"]);
                    $storage = test_input($_POST["storage"]);
                    $os = test_input($_POST["os"]);
                    
                    //setting the vars for uploading the image       
                    $name= $_FILES['file']['name'];
                    $tmp_name= $_FILES['file']['tmp_name'];
                    $size= $_FILES['file']['size'];
                    $position= strpos($name, ".");
                    $fileextension= substr($name, $position + 1);
                    $fileextension= strtolower($fileextension);

                    //checking if there is a name
                    if (isset($name)) 
                    {
                        //setting a path
                        $path= getcwd().'\game_images\\';

                        if (empty($name))
                        {
                            echo "Please choose a file";
                        }
                        else if (!empty($name) && ($size < 20971520))
                        {
                            //checking the file type
                            if (($fileextension !== "jpg") && ($fileextension !== "jpeg") )
                            {
                                echo "The file extension must be .jpg or .jpeg in order to be uploaded";
                            }
                            else if (($fileextension == "jpg") || ($fileextension == "jpeg"))
                            {
                                //uploading the game here/////////////////////////////genre doenst work yet
                                $name = $DBGame->AddGame($title,$description,$releaseDate,$publisher,$cpu,$gpu,$ram,$storage,$os);

                                $DBGenre->AddGenre($genre, explode('.',$name)[0] );
                                if (move_uploaded_file($tmp_name, $path.$name)) 
                                {
                                    echo 'Uploaded!';
                                    header("Location: index.php");
                                }
                                else
                                {
                                    echo 'failed';
                                }
                            }
                        }
                        else
                        {
                            echo "The size of the file must be less than 20MB in order to be uploaded.";	
                        }
                    }
                        
                }
            }//not used
            else if (isset($_POST['add_genre']))
            {
                $genre = $_POST["genre"];
                if (in_array($genre, $genres) == false)
                {
                    array_push($genres,$genre);
                    $_SESSION[ "genre" ] = $genres;
                }
            }
        }
    
        //Javascript script
        echo '<script type="text/javascript" src="javascript/formValidation.js"></script>';

        //forms
        echo '<div class="add_game">';
        echo '<h2> Add Game </h2>';
            echo '<div class="general_information">';
            echo '<h3> General Information</h3>';
            echo '<form name="addGameForm" action="#upload" method="post" enctype="multipart/form-data" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" onsubmit="return validateAddGameForm()">';

                echo '<a>Title:</a> <br>';
                echo '<input id="title" type="text" placeholder="Title" name="title">';
                echo '<span class="error">'.$titleErr.'</span><br>';
                
                echo '<a>Description:</a> <br>';
                echo '<textarea id="description" type="text" placeholder="Description" name="description" maxlenght="255"></textarea>';
                echo '<span class="error">'.$descriptionErr.'</span><br>';

                echo '<a>Add Picture:</a> <br>';
                echo '<input type="file" name="file"/>';
                echo '<span class="error">'.$piccErr.'</span><br>';

                echo '<a>Publisher:</a> <br>';
                echo '<input id="publisher" type="text" placeholder="Publisher" name="publisher">';
                echo '<span class="error">'.$publisherErr.'</span><br>';

                echo '<a>Release Date:</a> <br>';
                echo '<input id="releaseDate" type="date" placeholder="Release Date" name="releaseDate">';
                echo '<span class="error">'.$releaseDateErr.'</span><br>';

                echo '<a>Genre:</a> <br>';
                echo '<select id="genre" name="genre">';
                    //hardcoded for now bc reasons lol also doens
                    echo '<option value="action">action</option>';
                    echo '<option value="adventure">adventure</option>';
                    echo '<option value="puzzle">puzzle</option>';
                    echo '<option value="rhythm">rhythm</option>';
                    echo '<option value="rpg">rpg</option>';
                    echo '<option value="shooter">shooter</option>';
                    echo '<option value="strategy">strategy</option>';
                echo '</select>';
                echo '<span class="error">'.$releaseDateErr.'</span><br>';
                // to much work for now to do it the way I wanted to I would need to save all the variables
                /*echo '<input type="submit" name="add_genre" value="Add Genre"><br>';
                if (count($genres) >= 1)
                {
                    echo '<a>Selected Genres:</a> <br>';
                    foreach ($genres as $val)
                    {
                        echo $val. "<br>";
                    }
                }*/
                
            
            echo '</div>';

            echo '<div class="system_requirements">';
            echo '<h3> System Requirements</h3>';
                echo '<a>CPU:</a> <br>';
                echo '<input id="component" type="text" placeholder="CPU" name="cpu">';
                echo '<span class="error">'.$compErr.'</span><br>';

                echo '<a>GPU:</a> <br>';
                echo '<input id="component" type="text" placeholder="GPU" name="gpu">';
                echo '<span class="error">'.$compErr.'</span><br>';

                echo '<a>RAM:</a> <br>';
                echo '<input id="component" type="text" placeholder="RAM" name="ram">';
                echo '<span class="error">'.$compErr.'</span><br>';

                echo '<a>Storage:</a> <br>';
                echo '<input id="component" type="text" placeholder="Storage" name="storage">';
                echo '<span class="error">'.$compErr.'</span>';
                echo '<a>GB</a><br>';

                echo '<a>OS:</a> <br>';
                echo '<input id="component" type="text" placeholder="OS" name="os">';
                echo '<span class="error">'.$compErr.'</span><br>';

                echo '<br><input type="submit" name="submit" value="Submit"><br><br>';
            echo '</form>';
            echo '</div>';
        echo '</div>';
        
        
    }
    else
    {
        //cant acces this page if you are logged in
        header("Location: my-account.php");
        exit();
    }



    //Footer
    include_once 'include/footer.php';
?> 