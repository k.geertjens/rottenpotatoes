<?php 
    //misc
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_errors', true );

    //Header
    include_once 'include/header.php';

    AutoLoad('DBGame.php');
    AutoLoad('GameManager.php');

    //setting the database
    $DBgame = new DBGame();

    if (isset($_SESSION['action']))
    {
        $action = $_SESSION['action'];
    }
    else
    {
        $action = "new";
    }

    if (isset($_SESSION['game_id']))
    {
        $game_id = $_SESSION['game_id'];
    }
    else
    {
        $game_id = -1;
    }

    //Body
    if($user->getId() != -1 && $user->IsAdmin())
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            if(isset($_POST["new"]) && $action != "new")
            {
                $action = "new";
                $_SESSION['action'] = "new";
            }
            else if (isset($_POST["report"]) && $action != "report")
            {
                $action = "report";
                $_SESSION['action'] = "report";
            }
            else if (isset($_POST["reject"]))
            {
                $DBgame->RemoveGame($game_id);
                $_SESSION['game_id'] = -1;
            }
            else if (isset($_POST["dismiss"]))
            {
                $DBgame->DismissReportedGame($game_id);
                $_SESSION['game_id'] = -1;
            }
            else if (isset($_POST["approve"]))
            {
                $DBgame->ApproveGame($game_id);
                $_SESSION['game_id'] = -1;
            }
            else if (isset($_POST["remove"]))
            {
                $DBgame->RemoveGame($game_id);
                $_SESSION['game_id'] = -1;
            }
        }

        

        //Selecting a specific game from the dataset
        if (isset($_GET["game_id"]))
        {
            $game_id = $_GET["game_id"];
            $game = $DBgame->GetGame($game_id);
            if($game != null)
            {   
                $temp = $DBgame->IsApproved($game_id);

                $_SESSION['game_id'] = $game_id;
                //butons
                echo '<div class="user_request">';
                echo '<h2>User Requests</h2>';
                echo '<form method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">';

                if ($temp == false)
                {
                    echo '<h4>Approve or reject the new game.</h4>';
                    echo '<input class="button" type="submit" name="back" value="go back">';
                    echo '<input class="button" type="submit" name="reject" value="reject">';
                    echo '<input class="button" type="submit" name="approve" value="approve">';
                }
                else
                {
                    echo '<h4>remove or dismiss the report</h4>';
                    echo '<input class="button" type="submit" name="back" value="go back">';
                    echo '<input class="button" type="submit" name="dismiss" value="dismiss">';
                    echo '<input class="button" type="submit" name="remove" value="remove">';
                }
                
                echo '</form>';
                echo '</div>';

                //Image
                $image = "game_images/".$game->getImage();
                echo "<div class='game_left'>";
               
                echo "<div class='game_image'>";
                echo "<img src='$image' alt=''>";
                echo "</div>";
               
                //System specs
                echo "<div class='game_specs'>";
                echo "<h4>System Requirements</h4>";
                echo "<p>CPU: ".$game->getCPU()."</p>";
                echo "<p>GPU: ".$game->getGPU()."</p>";
                echo "<p>RAM: ".$game->getRAM()."</p>";
                echo "<p>Disk Space: ".$game->getDiskSpace()."</p>";
                echo "<p>OS: ".$game->getOS()."</p>";
                echo "</div>";
                echo "</div>";

                echo"<div class='game_right'>";
                
                //Title
                echo "<div class='game_title'>";
                echo "<h1>".$game->getTitle()."</h1>";
    
                //Sub-title
                echo "<div class='game_rating'>";
                echo "<b>Publisher: ".$game->getPublisher()."</b>";
                echo "<b>Release Date: ".date_format($game->getReleaseDate(), 'd-m-Y')."</b>";
                echo "</div>";
                echo "</div>";
    
                //Description
                echo "<p>".$game->getDescription()."</p>";
                echo "</div>";
                

                $DBReport = new DBReport();////////////////////for the reports
                $userManager = new UserManager();
                $reports = $DBReport->GetReports($game_id);
                echo "<div class= 'gameReviews'>";
                foreach($reports as $report)
                {//may have copied this idk tho
                    $image = $userManager->GetUserById($report->getUser())->getImage();

                    echo "<div class='review'>";

                    echo '<div class="reviewImage">';
                    echo "<img src='".$image."' alt='src='user_images/user.jpg'>";
                    echo "</div>";

                    echo "<div class='reviewBody'>";

                    echo "<p><i>".$userManager->GetUserById($report->getUser())->GetUserName()."</i></p>";
                    echo '<br>';
                    echo "<p>".$report->GetReport()."</p>";

                    echo "</div>";
                    echo "</div>";
                }
                echo "</div>";
                echo "</div>";
                
                
            }
            else
            {
                echo 'game has not been found';
            }
        }
        else if ($action == "new")
        {
            $games = $DBgame->GetNewGames();
            LoadGames($games,$action );
        }
        else if ($action == "report")
        {
            $games = $DBgame->GetReportedGames();
            LoadGames($games, $action);
        }
    }
    else
    {
        //can only acces this page if you are logged in
        header("Location: my-account.php");
        exit();
    }

    function LoadGames(array $games, string $action)
    { //schamelessly copied this from index.php sorry about that
        //buttons
        if ($action == "new")
        {
            $task = "new games waiting for approval";
        }
        else
        {
            $task = "reported games waiting to be looked at:";
        }
        echo '<div class="user_request">';
        echo '<h2>User Requests </h2>';
        echo '<h4>'.$task.' </h4>';
        echo '<form method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">';
        echo '<input class="button" type="submit" name="new" value="new games">';
        echo '<input class="button" type="submit" name="report" value="reported games">';
        echo '</form>';
        echo '</div>';


        $count = 0;
        foreach($games as $game)
        {
            //every 2 games is a new row
            if ($count %2 == 0)
            {
                echo'<div class="row">';
            }

            echo '<div class= "game-box">';
            echo '<div class= "homepage_img">';

            echo '<img src="game_images/'.$game->getImage().'"/>';
            echo '</div>';

            echo '<div class="card_description">';
            echo '<h2>'.$game->getTitle().'</h2>';
            //echo '<b>'.$game->getPublisher().'</b> <br>';
            //echo '<b>'.$game->getReleaseDate()->format('d-m-Y').'</b> <br>';
            
            $description = substr($game->GetDescription(), 0, 300)."...";
            echo '<p>'.$description.'</p>';
            echo '</div>';

            echo "<div class='ReadMore'>";///////////////////////////////////////////
            echo '<a href="user-requests.php?game_id='.$game->getId().'">details</a>';
            echo '</div>';

            echo '</div>';
            echo "<br>";

            //every 2 games is a new row end tag
            if ($count %2 == 1)
            {
                echo'</div>';
                echo'<br>';
            }
            $count++;
        }
    }
    echo "</div>";
    //Footer
    include_once 'include/footer.php';
?> 