-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 30 mei 2021 om 23:13
-- Serverversie: 10.4.17-MariaDB
-- PHP-versie: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rottenpotatoes`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `game`
--

CREATE TABLE `game` (
  `id` int(11) NOT NULL,
  `title` varchar(75) NOT NULL,
  `description` text NOT NULL,
  `releaseDate` date NOT NULL,
  `publisher` varchar(75) NOT NULL,
  `image` varchar(100) NOT NULL,
  `cpu` varchar(50) NOT NULL,
  `gpu` varchar(50) NOT NULL,
  `ram` varchar(20) NOT NULL,
  `diskSpace` varchar(10) NOT NULL,
  `os` varchar(30) NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `reported` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `game`
--

INSERT INTO `game` (`id`, `title`, `description`, `releaseDate`, `publisher`, `image`, `cpu`, `gpu`, `ram`, `diskSpace`, `os`, `approved`, `reported`) VALUES
(1, 'Rome: Total War', '<p>Rome: Total War is a strategy game, created in 2004 by Creative Assembly. \r\n This is the third installment within the Total War series. The game is often seen as one of the best strategy games, and even one of the best PC games of all time.</p><br> <h3>Gameplay</h3><p>The gameplay can be divided into two modes: <i>The Strategy Map</i>, where the player can manage cities and units alike in a turn-based fashion, and <i>The tactical Map</i>, where the player can fight real-time battles. </p> <br><h3>Nations</h3><p>The game offers a wide variety of nations to play. However, at the start the player is limited to the three roman families:</p><ul><li>Julii</li><li>Brutii</li><li>Scipii</li></ul><br> <p>When the player defeats another nation they become playable in future campaigns. Every nation has a set of unique millitary units and buildings to use, making for excellent replaybabilty.</p>', '2004-07-22', 'Creative Assembly', 'rome.jpg', 'Intel Pentium III', 'NVIDIA Geforce 6800 GT', '256 MB', '2.9 GB', 'Windows XP', 1, 0),
(2, 'Dark Souls', 'Nowadays, Dark Souls has somewhat of a cult following, generally being known as \'That hard game\'. Dark Souls has the player playing as the \'Chosen Undead\', who must venture to the land of Lordran to relink the First Flame.', '2011-07-22', 'From Software', 'darkSouls.jpg', 'Intel Core 2 Duo E6850', 'NVIDIA Geforce 9800 GTX+', '2 GB', '4 GB', 'Windows XP', 1, 0),
(3, 'Minecraft', 'Minecraft is a video game in which players create and break apart various kinds of blocks in three-dimensional worlds. The game\'s two main modes are Survival and Creative. In Survival, players must find their own building supplies and food. They also interact with blocklike mobs, or moving creatures.', '2011-11-18', 'mojang', 'Minecraft.jpg', 'Intel Core i3-3210', 'NVIDIA GeForce 400 series', '4 GB', '1 GB', 'Windows 7', 1, 0);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `game_genre`
--

CREATE TABLE `game_genre` (
  `game_id` int(11) NOT NULL,
  `genre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `game_genre`
--

INSERT INTO `game_genre` (`game_id`, `genre`) VALUES
(1, 'rpg'),
(1, 'strategy');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `genre`
--

CREATE TABLE `genre` (
  `genre` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `genre`
--

INSERT INTO `genre` (`genre`) VALUES
('action'),
('adventure'),
('puzzle'),
('rhythm'),
('rpg'),
('shooter'),
('strategy');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `report`
--

CREATE TABLE `report` (
  `ID` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  `Game_ID` int(11) NOT NULL,
  `Report` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `title` varchar(75) NOT NULL,
  `description` text NOT NULL,
  `rating` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `review`
--

INSERT INTO `review` (`id`, `user_id`, `game_id`, `title`, `description`, `rating`) VALUES
(1, 1, 1, 'Absolutely Amazing!', 'Rome: Total War is an absolutely amazing game, especially considering it\'s age. The real-time combat is extremely detailed and the turn-based campaign makes min-maxing extremely enjoyable. The feeling and atmosphere feels very natural, and that in combination with the soundtrack completely immerses you into a game that will have you playing for hours on end.', 5),
(2, 2, 1, 'Provides hours of enjoyment.', 'An amazing strategy game, especially considering its age. The depth that the variety of nations and units provides ensures that there is always something to learn and to improve upon. The campaign provides endless hours of enjoyment. The only negative that I could notice was the clearly dated graphics and somewhat clunky controls, but this doesn\'t distract from the gameplay all too much. Would definitely recommend!', 4),
(11, 9, 1, 'Pretty decent', 'Pretty decent game. The quality obviously isn\'t up to par with modern games, seeing as it\'s quite old. However, the gameplay is fairly enjoyable, although a bit hard at times to learn.', 3),
(12, 9, 2, 'Pretty hard, but cool once you get the hang of it', 'This game certainly lives up to its expectations. It\'s extremely hard to learn in the beginning, but perseverance will be your best friend. Don\'t give up, and you\'ll get far, I guarantee it.', 4),
(13, 1, 3, 'One of my favourites', 'I really love this game, and have played it for many hours during my childhood. You can do anything that your heart desires without being restricted by the game all too much. Playing with friends is especially enjoyable.', 5);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(75) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Gegevens worden geëxporteerd voor tabel `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`, `isAdmin`, `image`) VALUES
(1, 'K.Geertjens@gmail.com', 'KevinGeertjens', '1234', 1, '1.jpg'),
(2, 'joep.frankemolen@gmail.com', 'JoepFrankemolen', '4321', 1, 'user.jpg'),
(9, 'bob@gmail.com', 'Bob123', '4321', 0, 'user.jpg');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `game_genre`
--
ALTER TABLE `game_genre`
  ADD PRIMARY KEY (`game_id`,`genre`),
  ADD KEY `genre` (`genre`);

--
-- Indexen voor tabel `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`genre`);

--
-- Indexen voor tabel `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Game_ID` (`Game_ID`),
  ADD KEY `User_ID` (`User_ID`);

--
-- Indexen voor tabel `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`game_id`),
  ADD KEY `game_id` (`game_id`);

--
-- Indexen voor tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `game`
--
ALTER TABLE `game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT voor een tabel `report`
--
ALTER TABLE `report`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT voor een tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Beperkingen voor geëxporteerde tabellen
--

--
-- Beperkingen voor tabel `game_genre`
--
ALTER TABLE `game_genre`
  ADD CONSTRAINT `game_genre_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  ADD CONSTRAINT `game_genre_ibfk_2` FOREIGN KEY (`genre`) REFERENCES `genre` (`genre`);

--
-- Beperkingen voor tabel `report`
--
ALTER TABLE `report`
  ADD CONSTRAINT `report_ibfk_1` FOREIGN KEY (`Game_ID`) REFERENCES `game` (`id`),
  ADD CONSTRAINT `report_ibfk_2` FOREIGN KEY (`User_ID`) REFERENCES `user` (`id`);

--
-- Beperkingen voor tabel `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `review_ibfk_1` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`),
  ADD CONSTRAINT `review_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
