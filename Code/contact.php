<?php 
    //Including classes
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_errors', true );

    //PHPMailer
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;
    require 'libs/PHPMailer-master/src/Exception.php';
    require 'libs/PHPMailer-master/src/PHPMailer.php';
    require 'libs/PHPMailer-master/src/SMTP.php';

    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Mailer = "smtp";

    //$mail->SMTPDebug  = 1;  
    $mail->SMTPAuth   = TRUE;
    $mail->SMTPSecure = "tls";
    $mail->Port       = 587;
    $mail->Host       = "smtp.gmail.com";
    $mail->Username   = "rottenpotatoes.service@gmail.com";
    $mail->Password   = "Portal_21";

    //Header
    include_once 'include/header.php';

    //Handling POST input
    if(isset($_POST['email']) && isset($_POST['message']) && isset($_POST['subject']))
    {
        $subject = $_POST['subject'];
        $message = $_POST['message'];

        $sender = test_input($_POST["email"]);
        if (!filter_var($sender, FILTER_VALIDATE_EMAIL)) 
        {
            echo "Invalid email format";
        }
        else if($sender != "" && $subject != "" && $message != "")
        {
            $mail->IsHTML(true);
            $mail->AddAddress("rottenpotatoes.service@gmail.com", "Rotten Potatoes");
            $mail->SetFrom("$sender", "$sender");
            $mail->Subject = $subject;
            $content = "<p>$message</p>";

            $mail->MsgHTML($content); 
            if(!$mail->Send()) 
            {
                var_dump($mail);
            } 
            else 
            {
            }
        }
    }
?>

<script type="text/javascript" src="javascript/formValidation.js"></script>

<br>
<br>

<h2>Contact Us</h2> 
<br>
<p>Want to ask a question or leave some feedback? You've come to the right place! Leave your e-mail address and your message down below, and we'll get back to you as soon as we can.</p>

<br>
<br>

<form name="contactForm" action="contact.php" method="post" onsubmit="return validateContactForm()">
    <p>Your e-mail address:</p><input type="text" name="email" id="email">
    
    <br>
    <br>

    <p>Subject:</p><input type="text" id="subject" name="subject">

    <br>
    <br>

    <p>Your message:</p><textarea id="message" name="message"></textarea>

    <br>
    <input type="submit" name="submit" id="submit">
</form>

<?php
    //Footer
    include_once 'include/footer.php';
?> 

