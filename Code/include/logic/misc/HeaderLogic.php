<?php
    require_once "include/logic/classes/UserManager.php";
    
    //starting the session
    if (session_status() == 0)
    {
        
    }
    
    session_start();
    //setting the usermanager
    $Uman = new UserManager();

    //setting the user
    if (isset($_SESSION['email']) && isset($_SESSION['id'])) 
    {
        
        $user = $Uman->GetUserById(intval($_SESSION['id']));
        if ($user != null)
        {
            $_SESSION[ "email" ] = $user->getEmail();
            $_SESSION[ "id" ] = $user->getId();
    
            //
            if (!file_exists($user->getImage())) 
            {
                $user->setImage(DEFAULT_PIC);
            }
            
        }
        else
            {
                $user = new User(-1, "", "", -1, DEFAULT_PIC, "");
            }
        
    }
    else 
    {
        $user = new User(-1, "", "", -1, DEFAULT_PIC, "");
    }

?>