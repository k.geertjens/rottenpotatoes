<?php
    class Game
    {
        //Properties
        private $id;

        private $title;
        private $description;
        private $releaseDate;
        private $publisher;
        private $image;
    
        private $cpu;
        private $gpu;
        private $ram;
        private $diskSpace;
        private $os;

        //Constructor
        public function __construct(int $id, string $title, string $description, DateTime $releaseDate, string $publisher, string $image, string $cpu, string $gpu, string $ram, string $diskSpace, string $os)
        {
            $this->id = $id;

            $this->title = $title;
            $this->description = $description;
            $this->releaseDate = $releaseDate;
            $this->publisher = $publisher;
            $this->image = $image;
            
            $this->cpu = $cpu;
            $this->gpu = $gpu;
            $this->ram = $ram;
            $this->diskSpace = $diskSpace;
            $this->os = $os;
        }

        //Get-methods
        public function getId() { return $this->id; }
        
        public function getTitle() { return $this->title; }
        public function getDescription() { return $this->description; }
        public function getReleaseDate() { return $this->releaseDate; }
        public function getPublisher() { return $this->publisher; }
        public function getImage() { return $this->image; }

        public function getCPU() { return $this->cpu; }
        public function getGPU() { return $this->gpu; }
        public function getDiskSpace() { return $this->diskSpace; }
        public function getRAM() { return $this->ram; }
        public function getOS() { return $this->os; }

        //Methods
        public function ToString() 
        { 
            return "(Game) Title: $this->title, Publisher: $this->publisher";
        }
    }
?>