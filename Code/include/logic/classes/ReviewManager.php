<?php
    require_once 'include/logic/misc/Misc.php';
    require_once 'include/logic/classes/Review.php';
    require_once 'include/data/DBReview.php';
    AutoLoad('Review.php');
    AutoLoad('DBReview.php');

    class ReviewManager
    {
        //Properties
        private $reviews;
        private $DBHelper;

        //Constructor
        public function __construct()
        {
            $this->reviews = array();
            $this->DBHelper = new DBReview();
        }

        //Get-Methods
        public function GetReviews() { return $this->reviews; }
        public function GetReview(int $id) 
        { 
            foreach($this->reviews as $review)
            {
                if($reviews->GetId() == $id) return $review;
            }
            return null;
        }

        //Queries
        public function LoadReviewsByGame(int $game_id)
        {
            //Re-instatiating array
            $this->reviews = $this->DBHelper->GetReviewsByGame($game_id);
        }

        public function LoadReviewsByUser(int $user_id)
        {
            //Re-instatiating array
            $this->reviews = $this->DBHelper->GetReviewsByUser($user_id);
        }

        public function GetAvgRating(int $game_id)
        {
            return $this->DBHelper->GetAvgRating($game_id);
        }

        public function AddReview(int $user_id, int $game_id, string $title, string $description, int $rating)
        {
            $this->DBHelper->AddReview($user_id, $game_id, $title, $description, $rating);
        }
    }
?>