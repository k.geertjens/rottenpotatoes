<?php
    require_once dirname(__DIR__).'/logic/misc/Misc.php';
    AutoLoad('User.php');

    class DBUser
    {
        private $username = USERNAME;
        private $password = PASSWORD;
        private $conn_string = CONN_STRING;

        public function GetLoginUser(string $email, string $password2)
        {
            $user = null;

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                $query = "SELECT * FROM user WHERE email = :email AND password = :password";

                //Connection established, getting data
                $sth = $conn->prepare($query);

                $sth->execute([':email' => $email, ':password' => $password2]);
                foreach($sth->fetchall() as $row)
                {
                    if($row['id'] != "")
                    {
                        $user = new user(intval($row['id']), $row['email'], $row['username'],intval($row['isAdmin']) , "user_images/".$row["image"], "");
                    }
                    else
                    {
                        $user = null;
                    }
                }
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
            
            return $user;
        }

        public function GetUserById(int $id)
        {
            $user = null;

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                $query = "SELECT * FROM user WHERE id = :id" ;

                //Connection established, getting data
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);
                foreach($sth->fetchall() as $row)
                {
                    if($row['id'] != "")
                    {
                        $user = new user(intval($row['id']), $row['email'], $row['username'],intval($row['isAdmin']) , "user_images/".$row["image"], "");

                    }
                    else
                    {
                        $user = new user(-1, "", "", -1, "user_images/user.jpg", "");
                    }
                }
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
            
            return $user;
        }

        public function UpdateUser(int $id ,string $email, string $username2)
        {
            $user = null;

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                $query = "UPDATE user SET email = :email , username = :username WHERE id = :id" ;

                $sth = $conn->prepare($query);

                $sth->execute([':email' => $email, ':username' => $username2, ':id' => $id ]);


                //I have to do this for now its not a great selution 
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                $query = "SELECT * FROM user WHERE id = :id" ;

                //Connection established, getting data
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);
                foreach($sth->fetchall() as $row)
                {
                    if($row['id'] != "")
                    {
                        $user = new user(intval($row['id']), $row['email'], $row['username'],intval($row['isAdmin']) , "user_images/".$row["image"], "");

                    }
                    else
                    {
                        $user = new user(-1, "", "", -1, "user_images/user.jpg", "");
                    }
                }
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
            
            return $user;
        }

        public function AddUser(string $username, string $email, string $password)
        {

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                $query = "INSERT INTO user (email, username, password, isAdmin, image) VALUES (:email, :username, :password, 0, 'user.jpg') " ;
                
                $sth = $conn->prepare($query);

                $sth->execute([':email' => $email, ':username' => $username, ':password' => $password]);
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
            
        }

        public function UserEmailExists(string $email)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                $query = 'SELECT * FROM user WHERE email = :email';
                
                $sth = $conn->prepare($query);

                $sth->execute([':email' => $email]);
                //Connection established, getting data

                
                $row = $sth->fetch();
                if ( ! $row)
                {
                    return false;
                }
                else{
                    return true;
                }
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }

        public function CheckPassword(string $password2, int $id)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                $query = 'SELECT * FROM user WHERE id = :id and password = :password';
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id, ':password' => $password2]);
                //Connection established, getting data

                
                $row = $sth->fetch();
                if ( ! $row)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }

        public function UpdateUserImage(int $id, String $name)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                $query = 'UPDATE user SET image = :image WHERE id = :id';
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id, ':image' => $name]);
                //Connection established, getting data

                
                $row = $sth->fetch();
                if ( ! $row)
                {
                    return false;
                }
                else{
                    return true;
                }
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }
    }
?>