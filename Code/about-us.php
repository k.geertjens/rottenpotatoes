<?php 
    //Header
    include_once 'include/header.php';
?>

    <h2>About Us</h2>
    <div class="about_us">
        <div class="portrait">
            <img src="images/KevinPortrait.jpg" alt="">
            <h4>Kevin Geertjens</h4>
        </div>
        
        <div class="portrait">
            <img src="images/JoepPortrait.png" alt="">
            <h4>Joep Frankemolen</h4>
        </div>

        <br>

        <div class="about_us_description">
            <h4>The Website</h4>
            <p>Hey there, and welcome to RottenPotatoes, the game review website for you! You can find information about all your favourite games on this website, as well as
                dozens of user reviews, and you can add to that collection too! If you want to contribute, all you have to do is register a new account and either add a new game
                by navigating to the "New Game" page found in the footer, or navigate to a game page of your choosing and leave a review! 
            </p>

            <br>

            <h4>The Team</h4>
            <p>Our team consists of only two members: Kevin Geertjens and Joep Frankemolen. Both are Dutch
                ICT students attending the Fontys University of Applied Sciences in Eindhoven, The Netherlands. This website was developed during the second semester as a part of the 
                Web Application Development course, covering mainly PHP and JavaScript.</p>
        </div>
    </div>

<?php
    //Footer
    include_once 'include/footer.php';
?> 
