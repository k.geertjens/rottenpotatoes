<?php 
    //misc
    ini_set( 'error_reporting', E_ALL );
    ini_set( 'display_errors', true );

    //Header
    include_once 'include/header.php';

    //classes
    AutoLoad('Game.php');
    AutoLoad('GameManager.php');
    AutoLoad('Review.php');
    AutoLoad('ReviewManager.php');
    
    $UMan = new UserManager();
    
    $error_msg = "";
    $action = "";

    echo '<script type="text/javascript" src="javascript/formValidation.js"></script>';

    //checking if the user is logged in or not
    if($user->getId() == -1)
    {
        //logging in using this idk
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            $email = test_input($_POST["email"]);
            $password = test_input($_POST["password"]);

            $user = $UMan->Login($email,$password);

            if($user != null)
            {
                $_SESSION[ "email" ] = $user->getEmail();
                $_SESSION[ "id" ] = $user->getId();
                header("Refresh:0");
            }
            else
            {
                $error_msg = "wrong password or email";
            }
        }

        //html of the login menu
        echo '<div class="login">';

        echo '<form method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" name="loginForm" onsubmit="return validateLoginForm()">';
        echo '<a>Email</a> <br>';
        echo '<input id="Lemail" type="text" placeholder="email" name="email"><br>';
        echo '<a>Password</a> <br>';
        echo '<input id="Lpassword" type="text" placeholder="password" name="password"><br><br>';
        echo '<input type="submit" name="submit" value="Log in"><a class="error">'.$error_msg.'</a><br>';
        echo "<a id='toRegister' href='register.php'>dont have an account? register here</a>";
        echo '</form>';

        echo '</div>';
    }
    else
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            if(isset($_POST['LogOut']))
            {
                session_destroy();
                header("Location: my-account.php");
            }
            else if(isset($_POST['Edit']))
            {
                $action = "edit";
                
            }
            else if(isset($_POST['Save']))
            {
                if(!empty($_POST["password"]) && !empty($_POST["email"]))
                {
                    $password = test_input($_POST["password"]);
                    $email = test_input($_POST["email"]);
                    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) 
                    {
                        echo "Invalid email format";
                    }
                    else
                    {
                        $username = test_input($_POST["username"]);
                        $id = $user->getId();

                        //posebilety on multible emails lol could be a mayer thing bc we log in using emails so yeah oops
                        if($UMan->CheckPassword($password,$id))
                        {
                            $user = $UMan->UpdateUser($id,$email,$username);
                        }
                        else
                        {
                            $action = "edit";
                            $error_msg = "incorect password or email already in use";
                        }
                    }
                }
            }
            else if(isset($_POST['Back']))
            {
                $action = "";
                
            }
            else if(isset($_POST['AddPic']))
            {
                $action = "AddPic";
                
            }
            else if(isset($_POST['Upload']))
            {
                //setting the vars for uploading the image       
                $name= $_FILES['file']['name'];
                $tmp_name= $_FILES['file']['tmp_name'];
                $size= $_FILES['file']['size'];
                $position= strpos($name, ".");
                $fileextension= substr($name, $position + 1);
                $fileextension= strtolower($fileextension);

                //checking if there is a name
                if (isset($name)) 
                {
                    //setting a path
                    $path= getcwd().'\user_images\\';

                    if (empty($name))
                    {
                        echo "Please choose a file";
                    }
                    else if (!empty($name) && ($size < 20971520))
                    {
                        //checking the file type
                        if (($fileextension !== "jpg") && ($fileextension !== "jpeg") )
                        {
                            echo "The file extension must be .jpg or .jpeg in order to be uploaded";
                        }
                        else if (($fileextension == "jpg") || ($fileextension == "jpeg"))
                        {
                            //updating the image
                            $name = $user->getId().'.jpg';
                            $UMan->UpdateImage($user);
                            if (move_uploaded_file($tmp_name, $path.$name)) 
                            {
                                echo 'Uploaded!';
                                $action = "";
                                header("Refresh:0");
                            }
                            else
                            {
                                echo 'failed';
                            }
                        }
                    }
                    else
                    {
                        echo "The size of the file must be less than 20MB in order to be uploaded.";	
                    }
                }
            } 
        }
        
        echo '<div class= "my-account">';
        echo '<div class= "details">';
        echo '<h3>Personal Details</h3>';

        if ($action == "")
        {
            echo '<a>Username: '.$user->getUsername().'</a><br>';
            echo '<a>Email: '.$user->getEmail().'</a><br>';
            echo '<a>password:'.$user->getPassword().'</a>';

            echo '<form method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'">';
            echo '<input type="submit" name="Edit" value="Edit">';
            echo '<input type="submit" name="LogOut" value="Log Out">';
            echo '<input type="submit" name="AddPic" value="Add Picture">';
            echo '</form>';

        }
        else if ($action == "AddPic")
        {
            echo '<form action="#upload" method="post" enctype="multipart/form-data">';
            echo '<input type="file" name="file"/><br><br>';
            echo '<input type="submit" name="Upload" value="Upload"/>';
            echo '<input type="submit" name="Back" value="Back"/>';
            echo '</form>';
        }
        else
        {
            echo '<form method="post" action="'.htmlspecialchars($_SERVER["PHP_SELF"]).'" name="editAccountForm" onsubmit="return validateEditAccountForm()">';
            echo '<a>Username</a> <br>';
            echo '<input id="Lemail" type="text" placeholder="Username" name="username" value="'.$user->getUsername().'"><br>';
            echo '<a>Email</a> <br>';
            echo '<input id="Lemail" type="text" placeholder="Email" name="email" value="'.$user->getEmail().'"><br>';
            echo '<a>Password</a> <br>';
            echo '<input id="Lpassword" type="text" placeholder="Password" name="password" >'.$error_msg.'<br>';
            echo '<input type="submit" name="Save" value="Save">';
            echo '<input type="submit" name="Back" value="Back"><br><br>';
            echo '</form>';
        }
        
        echo '</div>';

        

        //the reviews
        $man = new ReviewManager();
        $man->LoadReviewsByUser($user->getId());
        $gman = new GameManager();
        $gman->LoadGames();

        echo '<div class= "reviews">';
        echo '<br><br><h3>Personal Reviews</h3>';
        echo '<p>Go <a href="my-reviews.php">here</a> to see your personal reviews</p>';
        echo '</div>';

        echo '</div>';
    }
    //Footer
    include_once 'include/footer.php';
?> 

