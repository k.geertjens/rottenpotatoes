<?php
    require_once dirname(__DIR__).'/logic/misc/Misc.php';
    AutoLoad('Game.php');

    AutoLoad('DBReport.php');

    class DBGame
    {
        private $username;
        private $password;
        private $conn_string;

        public function __construct()
        {
            $this->username = USERNAME;
            $this->password = PASSWORD;
            $this->conn_string = CONN_STRING;
        }

    
        //add a limit here so you dont load like 10k games at once or something
        public function GetGames()
        {
            $games = array();

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, loading games
                $query = "SELECT * FROM game where approved = true";
                foreach($conn->query($query) as $row)
                {
                    $game = new Game($row['id'], $row['title'], $row['description'], DateTime::createFromFormat('Y-m-d', $row['releaseDate']), $row['publisher'], $row['image'], 
                                    $row['cpu'], $row['gpu'], $row['ram'], $row['diskSpace'], $row['os']);

                    array_push($games, $game);
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning Array
            return $games;
        }

        public function GetNewGames()
        {
            $games = array();

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, loading games
                $query = "SELECT * FROM game where approved = false";
                foreach($conn->query($query) as $row)
                {
                    $game = new Game($row['id'], $row['title'], $row['description'], DateTime::createFromFormat('Y-m-d', $row['releaseDate']), $row['publisher'], $row['image'], 
                                    $row['cpu'], $row['gpu'], $row['ram'], $row['diskSpace'], $row['os']);

                    array_push($games, $game);
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning Array
            return $games;
        }

        public function GetReportedGames()
        {
            $games = array();

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, loading games
                $query = "SELECT * FROM game where reported = true";
                foreach($conn->query($query) as $row)
                {
                    $game = new Game($row['id'], $row['title'], $row['description'], DateTime::createFromFormat('Y-m-d', $row['releaseDate']), $row['publisher'], $row['image'], 
                                    $row['cpu'], $row['gpu'], $row['ram'], $row['diskSpace'], $row['os']);

                    array_push($games, $game);
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning Array
            return $games;
        }

        public function AddGame(string $title, string $description,string $releaseDate,string $publisher,string $cpu,string $gpu,string $ram,string $storage,string $os)
        {
            $games = array();

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);
                //Connection established, doing actions

                $query = "SELECT AUTO_INCREMENT
                from information_schema.TABLES
                WHERE TABLE_SCHEMA = 'rottenpotatoes'
                and TABLE_NAME = 'game'";

                $image = "0.jpg";

                foreach($conn->query($query) as $row)
                {
                    $image = $row['AUTO_INCREMENT'] .".jpg";
                }
                
                $query = "INSERT INTO `game` (`id`, `title`, `description`, `releaseDate`, `publisher`, `image`, `cpu`, `gpu`, `ram`, `diskSpace`, `os`, `approved`, `reported`) VALUES (NULL, :title, :description, :releasedate, :publisher, :image , :cpu, :gpu,:ram, :storage, :os, false, false) ";
                $sth = $conn->prepare($query);

                $sth->execute([':title' => $title, ':description' => $description, ':releasedate' => $releaseDate , ':publisher' => $publisher , ':image' => $image, ':cpu' => $cpu, ':gpu' => $gpu, ':ram' => $ram, ':storage' => $storage, ':os' => $os]);
                
                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning Array
            return $image;
        }

        public function GetGame(int $id)
        {
            $game = null;

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, loading game
                $query = "SELECT * FROM game where id = :id";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);
                foreach($sth->fetchall() as $row)
                {
                    $game = new Game($row['id'], $row['title'], $row['description'], DateTime::createFromFormat('Y-m-d', $row['releaseDate']), $row['publisher'], $row['image'], 
                                    $row['cpu'], $row['gpu'], $row['ram'], $row['diskSpace'], $row['os']);
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning Array
            return $game;
        }

        public function IsApproved(int $id)
        {
            $bool = false;

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, loading bool
                $query = "SELECT * FROM game where id = :id";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);
                foreach($sth->fetchall() as $row)
                {
                    $bool = $row['approved'];
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning bool
            return $bool;
        }

        public function IsReported(int $id)
        {
            $bool = false;

            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, loading bool
                $query = "SELECT * FROM game where id = :id";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);
                foreach($sth->fetchall() as $row)
                {
                    $bool = $row['reported'];
                }

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }

            //Returning bool
            return $bool;
        }

        public function RemoveGame(int $id)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                $DBReport = new DBReport();
                $DBReport->RemoveReports($id);

                //Connection established, deleting game
                $query = "DELETE FROM game WHERE id = :id ";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }

        public function ApproveGame(int $id)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, deleting game
                $query = "UPDATE game SET approved = true WHERE id = :id ";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }

        public function DismissReportedGame(int $id)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, deleting game
                $query = "UPDATE game SET reported = false WHERE id = :id ";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);

                //Closing Connection
                $conn = null;

                //updating the report table could have been done better with for ecample a report manager but eh this is good for now
                $DBReport = new DBReport();
                $DBReport->RemoveReports($id);
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }

        public function ReportGame(int $id)
        {
            try
            {
                $conn = new PDO($this->conn_string, $this->username, $this->password);

                //Connection established, deleting game
                $query = "UPDATE game SET reported = true WHERE id = :id ";
                $sth = $conn->prepare($query);

                $sth->execute([':id' => $id]);

                //Closing Connection
                $conn = null;
            }
            catch(PDOException $e)  
            {     
                echo $e->getMessage();
            }
        }
    }

?>