<?php
    require_once 'include/logic/misc/Misc.php';
    AutoLoad('User.php');
    AutoLoad('DBUser.php');

    class UserManager
    {
        private $DBHelper;

        public function __construct()
        {
            $this->DBHelper = new DBUser();
        }

        public function Login(string $username, string $password)
        {
            return $this->DBHelper->GetLoginUser($username, $password);
        }

        public function GetUserById($id)
        {
            return $this->DBHelper->GetUserById($id);
        }

        public function CheckPassword(String $password,int $id)
        {
            return $this->DBHelper->CheckPassword($password, $id);
        }

        public function UpdateUser(int $id ,string $email, string $username)
        {
            return $this->DBHelper->UpdateUser($id,$email,$username);
        }

        public function UpdateImage(User $user)
        {
            $name = $user->GetId().".jpg";
            $this->DBHelper->UpdateUserImage(strval($user->GetId()),$name);
            $user->SetImage($name);
        }

        public function CheckEmail(string $email)
        {
            return $this->DBHelper->UserEmailExists($email);
        }

        public function AddUser(String $username,STring $email,String $password)
        {
            return $this->DBHelper->AddUser($username,$email,$password);
        }
    }
?>