<?php
    require_once dirname(__DIR__).'/misc/Misc.php';
    AutoLoad('DBUser.php');
    class User
    {
        private $id;
        private $email;
        private $username;
        private $isAdmin;
        private $image;
        private $password;

        public function __construct(int $id, string $email, string $username, int $isAdmin, String $image, string $password)
        {
            $this->id = $id;
            $this->email = $email;
            $this->username = $username;
            $this->isAdmin = $isAdmin;
            $this->image = $image;
            $this->password = $password;

            
        }

        public function GetId() { return $this->id; }
        public function GetEmail() { return $this->email; }
        public function GetUsername() { return $this->username; }
        public function GetPassword() { return $this->password; }
        public function IsAdmin() { return $this->isAdmin; }
        public function GetImage() { return $this->image; }

        public function SetEmail(int $email) {$this->email = $email; }
        public function SetUsername(string $username) {$this->username = $username; }
        public function SetImage(String $image) {$this->image = $image; }
        public function SetPassword(int $password) {$this->password = $password; }

        

    }
?>